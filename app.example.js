class App {
    constructor() {
        this.clearButton = document.getElementById("clear-btn");
        this.loadButton = document.getElementById("load-btn");
        this.carContainerElement = document.getElementById("cars-container");
    }

    async init() {
        await this.load();

        // Register click listener
        this.clearButton.onclick = this.clear;
        this.loadButton.onclick = this.run;
    }


    run = () => {
        Car.list.forEach((car) => {
            const node = document.createElement('div');
            node.className = 'col-lg-4'
            node.innerHTML = car.render();
            this.carContainerElement.append(node);
        });
    };

    async filtererLoad(carCapacity) {
        console.log('car capacity ' + carCapacity)
        const cars = await Binar.listCars((item) => {
            return item.capacity > Number(carCapacity)
        })
        Car.init(cars)
    }


    async load() {
        this.find_capacity = document.getElementById("form_capacity").value;
        this.find_AvailableAt_date = document.getElementById("form_date").value;
        this.find_AvailableAt_time = document.getElementById()
        const cars = await Binar.listCars();
        Car.init(cars);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };
}