/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();
app.loadButton.onclick = app.filtererLoad(app.carCapacity)
console.log(app.carCapacity)

app.loadButton.addEventListener('click', function(params) {
    params.preventDefault()
    console.log(app.carCapacity)
    app.filtererLoad(app.carCapacity).then(app.run)
})

app.clearButton.addEventListener('click', () => {
    location.reload()
})


app.init().then(app.run);
